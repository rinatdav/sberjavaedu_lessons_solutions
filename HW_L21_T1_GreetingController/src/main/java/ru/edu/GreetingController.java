package ru.edu;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/author")
public class GreetingController {

    @GetMapping
    public ModelAndView aboutAuthor() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/author.jsp");
        mav.addObject("surname", "Давлитшин");
        mav.addObject("name", "Ринат");
        mav.addObject("patronymic", "Рауфович");
        mav.addObject("phone", "89101605354");
        mav.addObject("hobby", "спорт, изучение Java");
        mav.addObject("bbUrl", "https://bitbucket.org/dashboard/overview");
        return mav;
    }
}
