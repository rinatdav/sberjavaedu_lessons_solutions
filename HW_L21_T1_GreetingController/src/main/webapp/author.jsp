<%@page pageEncoding="UTF-8" %>

<html>
    <head>
        <title>Author</title>
    </head>
    <body>
        <div>
            <h1>Информация об авторе</h1>
            <p>Фамилия: <b>${surname}</b></p>
            <p>Имя: <b>${name}</b></p>
            <p>Отчество: <b>${patronymic}</b></p>
            <p>Телефон: <b>${phone}</b></p>
            <p>Хобби: <b>${hobby}</b></p>
            <p><a href=${bbUrl} target="_blank">Bitbucket url</a></p>
        </div>
    </body>
</html>