<%@page pageEncoding="UTF-8" %>

<html>
    <head>
        <style>
            .form {
            width: 38%;
            }
            input {
            float: right;
            margin-right: 10px;
            }
        </style>
    </head>
    <body>
        <div class="form">
            <form method="POST" action="/finance">
                <h1>Калькулятор доходности вклада</h1>
                <p><b>Сумма на момент открытия</b><input name="sum"></p>
                <p><b>Процентная ставка</b><input name="percentage"></p>
                <p><b>Количество лет</b><input name="years"></p>
                <p><input type="submit" value="Посчитать"></p>
            </form>
        </div>
    </body>
</html>

