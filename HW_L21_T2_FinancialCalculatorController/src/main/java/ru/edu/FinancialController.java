package ru.edu;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/finance")
public class FinancialController {

    @GetMapping
    public ModelAndView getStarterPage() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/financialCalculator.jsp");
        return mav;
    }

    @PostMapping
    public ModelAndView showResult(@RequestParam("sum") String sum,
                                   @RequestParam("percentage") String percentage,
                                   @RequestParam("years") String years) {

        int result;
        int minSum = 50000;

        ModelAndView mav = new ModelAndView();

        if (isNumeric(sum) || isNumeric(percentage) || isNumeric(years)) {
            mav.setViewName("/invalidDataFormat.jsp");
        } else if (Integer.parseInt(sum) < minSum) {
            mav.setViewName("/minimumAmount.jsp");
        } else {
            result = Integer.parseInt(sum);
            for (int i = 0; i < Integer.parseInt(years); i++) {
                result += result * Integer.parseInt(percentage) / 100;
            }
            mav.setViewName("/calculationResult.jsp");
            mav.addObject("result", result);
        }
        return mav;
    }

    private boolean isNumeric(String str) {

        try {
            return Integer.parseInt(str) < 0;
        } catch (NumberFormatException ex) {
            return true;
        }
    }
}
